const dotenv = require('dotenv');

dotenv.config();

const database = process.env.DATABASE_NAME;
const username = process.env.DATABASE_USER;
const password = process.env.DATABASE_ACCESS_KEY;
const host = process.env.DATABASE_HOST;
const port = process.env.DATABASE_PORT;

module.exports = { database, username, password, host, port };
