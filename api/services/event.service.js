const eventDao = require('../../db/dao/event.dao');
const { snakeToCamel, camelToSnake } = require('../../helpers/object.helper');

const getEventById = async (id) => {
  const event = await eventDao.getEventById(id);
  return snakeToCamel(event);
};

const createEvent = async (eventDto) => {
  const { oddsId, type, homeTeam, awayTeam, score, startAt } = eventDto;

  const createdEvent = await eventDao.createEvent(
    camelToSnake({
      oddsId,
      type,
      homeTeam,
      awayTeam,
      score,
      startAt,
    })
  );

  return snakeToCamel(createdEvent);
};

const updateEvent = async (id, event) => {
  const updatedEvent = await eventDao.updateEvent(id, camelToSnake(event));
  return snakeToCamel(updatedEvent);
};

module.exports = { getEventById, createEvent, updateEvent };
