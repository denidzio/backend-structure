const jwt = require('jsonwebtoken');
const { secret } = require('../../config/jwt.config');
const userDao = require('../../db/dao/user.dao');
const { snakeToCamel, camelToSnake } = require('../../helpers/object.helper');

const getAllUsers = () => {
  return userDao.getAllUsers();
};

const getUserById = async (id) => {
  const user = await userDao.getUserById(id);
  return snakeToCamel(user);
};

const createUser = async (userDto) => {
  const { type, email, phone, name, city } = userDto;

  const createdUser = await userDao.createUser({
    type,
    email,
    phone,
    name,
    city,
    balance: 0,
  });

  const accessToken = jwt.sign({ id: createdUser.id, type: createdUser.type }, secret);
  return { ...snakeToCamel(createdUser), accessToken };
};

const updateUser = async (id, user) => {
  const updatedUser = await userDao.updateUser(id, camelToSnake(user));
  return snakeToCamel(updatedUser);
};

module.exports = { getAllUsers, getUserById, createUser, updateUser };
