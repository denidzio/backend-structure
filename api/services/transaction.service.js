const transactionDao = require('../../db/dao/transaction.dao');
const { snakeToCamel, camelToSnake } = require('../../helpers/object.helper');

const createTransaction = async (transactionDto) => {
  const { userId, cardNumber, amount } = transactionDto;

  const createdTransaction = await transactionDao.createTransaction(
    camelToSnake({
      userId,
      cardNumber,
      amount,
    })
  );

  return snakeToCamel(createdTransaction);
};

module.exports = { createTransaction };
