const betDao = require('../../db/dao/bet.dao');
const { snakeToCamel, camelToSnake } = require('../../helpers/object.helper');

const getBetsByEventIdAndWin = async (eventId, win) => {
  const bets = await betDao.getBetsByEventIdAndWin(eventId, win);
  return bets.map(snakeToCamel);
};

const createBet = async (betDto) => {
  const { eventId, userId, betAmount, prediction, multiplier, win } = betDto;

  const createdBet = await betDao.createBet(
    camelToSnake({
      eventId,
      userId,
      betAmount,
      prediction,
      multiplier,
      win,
    })
  );

  return snakeToCamel(createdBet);
};

const updateBetById = async (id, bet) => {
  const updatedBet = await betDao.updateBetById(id, camelToSnake(bet));
  return snakeToCamel(updatedBet);
};

module.exports = { createBet, getBetsByEventIdAndWin, updateBetById };
