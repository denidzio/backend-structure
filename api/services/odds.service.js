const oddsDao = require('../../db/dao/odds.dao');
const { camelToSnake, snakeToCamel } = require('../../helpers/object.helper');

const getOddsById = async (id) => {
  const odds = await oddsDao.getOddsById(id);
  return snakeToCamel(odds);
};

const createOdds = async (oddsDto) => {
  const { homeWin, draw, awayWin } = oddsDto;

  const createdOdds = await oddsDao.createOdds(
    camelToSnake({
      homeWin,
      draw,
      awayWin,
    })
  );

  return snakeToCamel(createdOdds);
};

module.exports = { getOddsById, createOdds };
