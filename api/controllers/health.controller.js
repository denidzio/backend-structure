const getAllHealth = (req, res, next) => {
  try {
    return res.send('Hello World!');
  } catch (error) {
    next(error);
  }
};

module.exports = { getAllHealth };
