const HttpError = require('../errors/HttpError');
const betService = require('../services/bet.service');
const userService = require('../services/user.service');
const { getEventById } = require('../services/event.service');
const { getOddsById } = require('../services/odds.service');

const createBet = async (req, res, next) => {
  try {
    const user = await userService.getUserById(req.authPayload.id);

    if (!user) {
      throw new HttpError(400, 'User does not exist');
    }

    if (Number(user.balance) < Number(req.body.betAmount)) {
      throw new HttpError(400, 'Not enough balance');
    }

    const event = await getEventById(req.body.eventId);

    if (!event) {
      throw new HttpError(404, 'Event not found');
    }

    const odds = await getOddsById(event.oddsId);

    if (!odds) {
      throw new HttpError(404, 'Odds not found');
    }

    let multiplier;

    switch (req.body.prediction) {
      case 'w1':
        multiplier = odds.homeWin;
        break;
      case 'w2':
        multiplier = odds.awayWin;
        break;
      case 'x':
        multiplier = odds.draw;
        break;
    }

    const bet = await betService.createBet({ ...req.body, multiplier, eventId: event.id, userId: user.id });
    const currentBalance = user.balance - req.body.betAmount;

    await userService.updateUser(user.id, { balance: currentBalance });

    req.app.get('statEmitter').emit('newBet');

    return res.send({ ...bet, currentBalance });
  } catch (error) {
    next(error);
  }
};

module.exports = { createBet };
