const { stats } = require('../../stats/');

const getAllStats = (req, res, next) => {
  try {
    return res.send(stats);
  } catch (error) {
    next(error);
  }
};

module.exports = { getAllStats };
