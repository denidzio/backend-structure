const HttpError = require('../errors/HttpError');
const userService = require('../services/user.service');
const transactionService = require('../services/transaction.service');

const createTransaction = async (req, res, next) => {
  try {
    const user = await userService.getUserById(req.body.userId);

    if (!user) {
      throw new HttpError(400, 'User does not exist');
    }

    const transaction = await transactionService.createTransaction(req.body);
    const currentBalance = req.body.amount + user.balance;

    await userService.updateUser(user.id, { balance: currentBalance });

    return res.send({ ...transaction, currentBalance });
  } catch (error) {
    next(error);
  }
};

module.exports = { createTransaction };
