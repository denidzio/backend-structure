const eventService = require('../services/event.service');
const betService = require('../services/bet.service');
const userService = require('../services/user.service');
const { createOdds } = require('../services/odds.service');

const createEvent = async (req, res, next) => {
  try {
    const odds = await createOdds(req.body.odds);

    const event = await eventService.createEvent({
      ...req.body,
      oddsId: odds.id,
    });

    req.app.get('statEmitter').emit('newEvent');

    return res.send({ ...event, odds });
  } catch (error) {
    next(error);
  }
};

const updateEventById = async (req, res, next) => {
  try {
    const bets = await betService.getBetsByEventIdAndWin(req.params.id, null);
    const [w1, w2] = req.body.score.split(':');

    const multiplier = Number(w1) > Number(w2) ? 'w1' : Number(w1) < Number(w2) ? 'w2' : 'x';

    const event = await eventService.updateEvent(req.params.id, { score: req.body.score });

    for (const bet of bets) {
      if (bet.prediction === multiplier) {
        const user = await userService.getUserById(bet.userId);

        await betService.updateBetById(bet.id, { win: true });
        await userService.updateUser(user.id, { balance: user.balance + bet.betAmount * bet.multiplier });

        continue;
      }

      betService.updateBetById(bet.id, { win: false });
    }

    return res.send(event);
  } catch (error) {
    next(error);
  }
};

module.exports = { createEvent, updateEventById };
