const db = require('../../db/connection');
const HttpError = require('../errors/HttpError');

module.exports = async (uselessRequest, uselessResponse, next) => {
  try {
    await db.raw('select 1+1 as result');
    next();
  } catch {
    next(new HttpError(500, 'No db connection'));
  }
};
