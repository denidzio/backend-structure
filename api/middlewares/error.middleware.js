const HttpError = require('../errors/HttpError');

module.exports = (err, req, res, next) => {
  if (err instanceof HttpError) {
    return res.status(err.status).send({ error: err.message });
  }

  if (err.code === '23505') {
    return res.status(400).send({
      error: err.detail,
    });
  }

  return res.status(500).send({ error: 'Internal Server Error' });
};
