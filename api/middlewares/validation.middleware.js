module.exports = (schema, field) => (req, res, next) => {
  const validateResult = schema.validate(req[field]);

  if (validateResult.error) {
    res.status(400).send({ error: validateResult.error.details[0].message });
    return;
  }

  next();
};
