const jwt = require('jsonwebtoken');
const { secret } = require('../../config/jwt.config');

module.exports =
  (roles = [], options = {}) =>
  (req, res, next) => {
    // return res.send({
    //   token: jwt.sign({ type: 'client', id: '0f290598-1b54-4a36-8c58-33caa7d08b5f' }, process.env.JWT_SECRET),
    // });
    try {
      if (!req.headers.authorization) {
        throw new Error();
      }

      const token = req.headers.authorization.split(' ')[1];
      const payload = jwt.verify(token, secret);

      if (roles.length && !roles.includes(payload.type)) {
        throw new Error();
      }

      if (options.matchId && req.params.id !== payload.id) {
        return res.status(401).send({ error: 'UserId mismatch' });
      }

      req.authPayload = payload;

      next();
    } catch {
      res.status(401).send({ error: 'Not Authorized' });
    }
  };
