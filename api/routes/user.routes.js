const { Router } = require('express');

const userController = require('../controllers/user.controller');
const validation = require('../middlewares/validation.middleware');
const authorization = require('../middlewares/authorization.middleware');
const userSchemas = require('../schemas/user.schemas');

const router = Router();

router.get('/:id', validation(userSchemas.getByIdRequestSchema, 'params'), userController.getUserById);
router.post('/', validation(userSchemas.createUserRequestSchema, 'body'), userController.createUser);

router.put(
  '/:id',
  authorization([], { matchId: true }),
  validation(userSchemas.updateUserRequestSchema, 'body'),
  userController.updateUserById
);

module.exports = router;
