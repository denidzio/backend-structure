const { Router } = require('express');

const betController = require('../controllers/bet.controller');
const validation = require('../middlewares/validation.middleware');
const authorization = require('../middlewares/authorization.middleware');
const betSchemas = require('../schemas/bet.schemas');
const { CLIENT, ADMIN } = require('../roles');

const router = Router();

router.post(
  '/',
  authorization([CLIENT, ADMIN]),
  validation(betSchemas.createBetRequestSchema, 'body'),
  betController.createBet
);

module.exports = router;
