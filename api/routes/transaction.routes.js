const { Router } = require('express');

const transactionController = require('../controllers/transaction.controller');
const validation = require('../middlewares/validation.middleware');
const authorization = require('../middlewares/authorization.middleware');
const transactionSchemas = require('../schemas/transaction.shemas');
const { ADMIN } = require('../roles');

const router = Router();

router.post(
  '/',
  authorization([ADMIN]),
  validation(transactionSchemas.createTransactionRequestSchema, 'body'),
  transactionController.createTransaction
);

module.exports = router;
