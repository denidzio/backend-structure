const { Router } = require('express');

const statsController = require('../controllers/stats.controller');
const authorization = require('../middlewares/authorization.middleware');
const { ADMIN } = require('../roles');

const router = Router();

router.get('/', authorization([ADMIN]), statsController.getAllStats);

module.exports = router;
