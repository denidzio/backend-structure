const userRoutes = require('./user.routes');
const healthRoutes = require('./health.routes');
const eventRoutes = require('./event.routes');
const betRoutes = require('./bet.routes');
const statsRoutes = require('./stats.routes');
const transactionRoutes = require('./transaction.routes');

module.exports = (app) => {
  app.use('/users', userRoutes);
  app.use('/health', healthRoutes);
  app.use('/events', eventRoutes);
  app.use('/bets', betRoutes);
  app.use('/stats', statsRoutes);
  app.use('/transactions', transactionRoutes);
};
