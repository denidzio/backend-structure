const { Router } = require('express');

const router = Router();

const healthController = require('../controllers/health.controller');

router.get('/', healthController.getAllHealth);

module.exports = router;
