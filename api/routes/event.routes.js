const { Router } = require('express');

const eventController = require('../controllers/event.controller');
const validation = require('../middlewares/validation.middleware');
const authorization = require('../middlewares/authorization.middleware');
const eventSchemas = require('../schemas/event.schemas');
const { ADMIN } = require('../roles');

const router = Router();

router.post(
  '/',
  authorization([ADMIN]),
  validation(eventSchemas.createEventRequestSchema, 'body'),
  eventController.createEvent
);

router.put(
  '/:id',
  authorization([ADMIN]),
  validation(eventSchemas.updateEventRequestSchema, 'body'),
  eventController.updateEventById
);

module.exports = router;
