const { EventEmitter } = require('events');
const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');
const routes = require('./api/routes');
const databaseMiddleware = require('./api/middlewares/database.middleware');
const errorMiddleware = require('./api/middlewares/error.middleware');
const db = require('./db/connection');
const stats = require('./stats');
const checkEnv = require('./helpers/env.helper');

checkEnv().catch((missedVars) => {
  console.log('Missed some environment vars:', missedVars);
  process.exit(1);
});

db.raw('select 1+1 as result').catch(() => {
  console.log('Database is not available');
  process.exit(1);
});

const app = express();
const port = 3000;

const statEmitter = new EventEmitter();

app.set('statEmitter', statEmitter);
app.use(express.json());
app.use(databaseMiddleware);

app.use(
  expressWinston.logger({
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({ filename: './logs/info.log', level: 'info' }),
    ],
    format: winston.format.combine(
      winston.format.timestamp({ format: 'YYYY.MM.DD HH:mm:ss' }),
      winston.format.printf((info) => `${info.timestamp}: ${info.message}`)
    ),
    expressFormat: true,
  })
);

routes(app);

app.use(errorMiddleware);

const server = app.listen(port, () => {
  stats.listen(app);
  console.log(`App listening at http://localhost:${port}`);
});

process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
  console.log('Closing http server.');

  server.close(() => {
    console.log('Http server closed.');

    db.destroy(() => {
      console.log('PostgreSQL connection closed.');
      process.exit(0);
    });
  });
});

// Do not change this line
module.exports = { app };
