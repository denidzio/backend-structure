const db = require('../connection');

const table = 'user';

const getUserById = async (id) => {
  const user = await db(table).where('id', id).returning('*');
  return user.length ? user[0] : null;
};

const createUser = async (user) => {
  const createdUser = await db(table).insert(user).returning('*');
  return createdUser[0];
};

const updateUser = async (id, user) => {
  const updatedUser = await db(table).where('id', id).update(user).returning('*');
  return updatedUser.length ? updatedUser[0] : null;
};

module.exports = { getUserById, createUser, updateUser };
