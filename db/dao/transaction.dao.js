const db = require('../connection');

const table = 'transaction';

const createTransaction = async (transaction) => {
  const createdTransaction = await db(table).insert(transaction).returning('*');
  return createdTransaction[0];
};

module.exports = { createTransaction };
