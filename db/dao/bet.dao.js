const db = require('../connection');

const table = 'bet';

const getBetsByEventIdAndWin = async (eventId, win) => {
  return await db(table).where('event_id', eventId).andWhere('win', win);
};

const createBet = async (bet) => {
  const createdBet = await db(table).insert(bet).returning('*');
  return createdBet[0];
};

const updateBetById = async (id, bet) => {
  const updatedBet = await db(table).where('id', id).update(bet);
  return updatedBet.length ? updatedBet[0] : null;
};

module.exports = { createBet, getBetsByEventIdAndWin, updateBetById };
