const db = require('../connection');

const table = 'event';

const getEventById = async (id) => {
  const event = await db(table).where('id', id).returning('*');
  return event.length ? event[0] : null;
};

const createEvent = async (event) => {
  const createdEvent = await db(table).insert(event).returning('*');
  return createdEvent[0];
};

const updateEvent = async (id, event) => {
  const updatedEvent = await db(table).where('id', id).update(event).returning('*');
  return updatedEvent.length ? updatedEvent[0] : null;
};

module.exports = { getEventById, createEvent, updateEvent };
