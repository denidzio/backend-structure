const { snakeCaseToCamelCase, camelCaseToSnakeCase } = require('./string.helper');

const snakeToCamel = (body) => {
  if (!body) {
    return null;
  }

  return Object.fromEntries(Object.entries(body).map(([key, value]) => [snakeCaseToCamelCase(key), value]));
};

const camelToSnake = (body) => {
  if (!body) {
    return null;
  }

  return Object.fromEntries(Object.entries(body).map(([key, value]) => [camelCaseToSnakeCase(key), value]));
};

module.exports = { snakeToCamel, camelToSnake };
