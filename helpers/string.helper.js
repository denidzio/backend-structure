const snakeCaseToCamelCase = (string) => {
  return string
    .split('_')
    .map((piece, index) => (index ? piece[0].toUpperCase() + piece.slice(1) : piece))
    .join('');
};

const camelCaseToSnakeCase = (string) => {
  return string.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);
};

module.exports = { snakeCaseToCamelCase, camelCaseToSnakeCase };
