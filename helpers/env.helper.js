const dotenv = require('dotenv');

dotenv.config();

module.exports = (cb) => {
  const missedVars = [];

  if (!process.env.DATABASE_PORT) {
    missedVars.push('DATABASE_PORT');
  }

  if (!process.env.DATABASE_HOST) {
    missedVars.push('DATABASE_HOST');
  }

  if (!process.env.DATABASE_NAME) {
    missedVars.push('DATABASE_NAME');
  }

  if (!process.env.DATABASE_USER) {
    missedVars.push('DATABASE_USER');
  }

  if (!process.env.DATABASE_ACCESS_KEY) {
    missedVars.push('DATABASE_ACCESS_KEY');
  }

  if (!process.env.JWT_SECRET) {
    missedVars.push('JWT_SECRET');
  }

  if (missedVars.length) {
    return Promise.reject(missedVars);
    // cb(missedVars);
  }

  return Promise.resolve();
};
